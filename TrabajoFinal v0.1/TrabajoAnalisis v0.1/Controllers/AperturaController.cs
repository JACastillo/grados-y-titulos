﻿using SIGRAT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using TrabajoAnalisis_v0._1.Models;
using WebApplication3.Models;

namespace TrabajoAnalisis_v0._1.Controllers
{
    public class AperturaController : Controller
    {
        // GET: Apertura
        ConsultaSolicitud datosSolicitud;
        Verificacion c;
        static string usuario;

        public AperturaController()
        {
            datosSolicitud = new ConsultaSolicitud();
            c = new Verificacion();
        }
        public ActionResult Inicio()
        {
            System.Web.HttpContext.Current.Session["nombre"] = null;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            return View();
        }

        [HttpPost]
        public ActionResult Inicio(string Usuario, string Contraseña)
        {
            string Direccion = "";

            Usuario u;
            usuario = Usuario;
            u = new Usuario(Usuario, Contraseña);
            if (u.Ingresarusuario() && System.Web.HttpContext.Current.Session["nombre"] == null) {
                System.Web.HttpContext.Current.Session["nombre"] = u.Nombre2;
                if (u.Nivel.Equals("O"))
                {
                    Direccion = "NivelOperacional";
                } else if (u.nivel.Equals("X"))
                {
                    Direccion = "NivelExterno";
                } else if (u.Nivel.Equals("E"))
                {
                    Direccion = "NivelEstrategico";
                }
                else
                {
                    Direccion = "NivelTactico";
                }

            }
            else
            {
                Direccion = "Inicio";
            }
            return RedirectToAction(Direccion, "Apertura");
        }


        public ActionResult Salir()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            System.Web.HttpContext.Current.Session.Abandon();
            System.Web.HttpContext.Current.Session.Clear();
            return RedirectToAction("Inicio", "Apertura");
        }

        [HttpGet]
        public ActionResult ConsultarSolicitud()
        {
            if (System.Web.HttpContext.Current.Session["nombre"] != null) {
                return View(datosSolicitud);
            }
            else
            {
                return RedirectToAction("Inicio", "Apertura");
            }
        }

        [HttpPost]
        public ActionResult ConsultarSolicitud(string codigo)
        {
            if (System.Web.HttpContext.Current.Session["nombre"] != null) {
                datosSolicitud = new ConsultaSolicitud(codigo);
                var model = datosSolicitud;
                return View(model);
            }
            else
            {
                return RedirectToAction("Inicio", "Apertura");
            }
        }


        [HttpGet]
        public ActionResult VerificarRequisitos()
        {
            if (System.Web.HttpContext.Current.Session["nombre"] != null)
            {
                return View(c);
            }
            else
            {
                return RedirectToAction("Inicio", "Apertura");
            }
        }
        [HttpPost]
        public ActionResult VerificarRequisitos(string codigo, string requisitos)
        {
            if (System.Web.HttpContext.Current.Session["nombre"] != null)
            {
                c = new Verificacion(codigo,requisitos);
                return View(c);
            }
            else
            {
                return RedirectToAction("Inicio", "Apertura");
            }
        }
        
        public ActionResult SolicitudRegistrada()
        {
            return View(new IniciarSolicitudGradoModel(usuario));
        }



        public ActionResult NivelEstrategico()
        {
            if (System.Web.HttpContext.Current.Session["nombre"] != null)
            {

                //ViewBag.Mostrar = "display:none";
                ViewBag.Message = System.Web.HttpContext.Current.Session["nombre"];
                return View();

            }
            else
            {
                return RedirectToAction("Inicio", "Apertura");
            }
        }

        public ActionResult NivelTactico()
        {
            if (System.Web.HttpContext.Current.Session["nombre"] != null)
            {
                //ViewBag.Mostrar = "display:none";
                ViewBag.Message = System.Web.HttpContext.Current.Session["nombre"];
                return View();

            }
            else
            {
                return RedirectToAction("Inicio", "Apertura");
            }
        }

        public ActionResult NivelOperacional()
        {
            if (System.Web.HttpContext.Current.Session["nombre"]!=null) {

                //ViewBag.Mostrar = "display:none";
                ViewBag.Message = System.Web.HttpContext.Current.Session["nombre"];
                return View();

            }
            else
            {
                return RedirectToAction("Inicio","Apertura");
            }
        }

        public ActionResult NivelExterno()
        {
            if (System.Web.HttpContext.Current.Session["nombre"] != null)
            {

                ViewBag.Mostrar = "display:none";
                ViewBag.Message = System.Web.HttpContext.Current.Session["nombre"];
                return View();

            }
            else
            {
                return RedirectToAction("Inicio", "Apertura");
            }
        }

        public ActionResult IniciarSolicitud()
        {
            if (System.Web.HttpContext.Current.Session["nombre"]!=null) {
                return View();
            }
            else
            {
                return RedirectToAction("Inicio","Apertura");
            }
        }
    }
}