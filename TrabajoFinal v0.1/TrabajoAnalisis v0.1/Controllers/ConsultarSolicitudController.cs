﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication3.Controllers
{
    public class ConsultarSolicitudController : Controller
    {
        Models.ConsultaSolicitud datosSolicitud;

        public ConsultarSolicitudController()
        {
            datosSolicitud = new Models.ConsultaSolicitud();
        }

        [HttpGet]
        public ActionResult ConsultarSolicitud()
        {
            return View(datosSolicitud);
        }

        [HttpPost]
        public ActionResult ConsultarSolicitud(string codigo)
        {
            datosSolicitud = new Models.ConsultaSolicitud(codigo);
            var model = datosSolicitud;

            return View(model);
        }
    }
}
