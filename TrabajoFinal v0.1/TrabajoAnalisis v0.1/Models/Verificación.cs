﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using TrabajoAnalisis_v0._1.Generico;

namespace TrabajoAnalisis_v0._1.Models
{
    public class Verificacion
    {
        public List<Verifica> Verificar;
        public List<bool> Condic;
        public string codigo,Nombre;
        public GestionarBasedeDatos g;

        public Verificacion()
        {
            g = new GestionarBasedeDatos();
            Verificar = new List<Verifica>();
        }
        public Verificacion(string codigo,string Nombre)
        {
            try
            {
                g = new GestionarBasedeDatos();
                Verificar = new List<Verifica>();
                this.codigo = codigo;
                this.Nombre = Nombre;

                DataSet q = ObtenerPrimerDataset();
                ObtenerSegundoData(q);
                ValidarRequisitos(q);
                Condic = new List<bool>();
            }
            catch (Exception e) { }
        }
    
        public DataSet ObtenerPrimerDataset(){
            DataSet d = new DataSet();
            SqlDataAdapter usuario;
            SqlCommand comando;
            SqlParameter parametro;
            try
            {
                comando = new SqlCommand();
                comando.CommandType = CommandType.StoredProcedure;
                comando.CommandText = "Verificar_1";
                comando.Connection = g.Conectar();

                parametro = new SqlParameter("@Nombre", SqlDbType.Char);
                parametro.Value = Nombre;

                comando.Parameters.Add(parametro);

                usuario = new SqlDataAdapter(comando);
                usuario.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                usuario.Fill(d);
                g.Desconectar();
            }
            catch (SqlException e)
            {
            }
            return d;
        }
        public void ValidarRequisitos(DataSet q)
        {
            try
            {
                SqlConnection s;
                SqlCommand cmd = new SqlCommand();
                s = g.Conectar();
                int i = 0;

                int d = Verificar.Count();

                foreach (DataRow c in q.Tables[0].Rows)
                {
                    if (d > 0)
                    {
                        Verificar[i].Descripcion = c[1].ToString();
                        d--;
                    }
                    else
                    {
                        Verifica v = new Verifica();
                        Verificar.Add(v);
                        Verificar[i].Descripcion = c[1].ToString();
                    }
                    i++;
                }
                g.Desconectar();
            }
            catch (Exception e)
            {}
        }
        public void ObtenerSegundoData(DataSet q)
        {
            DataSet d = new DataSet();
            SqlDataAdapter usuario;
            SqlCommand comando;
            SqlParameter parametro;
            int i=0;
            try
            {
                foreach (DataRow c in q.Tables[0].Rows)
                {
                    comando = new SqlCommand();
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.CommandText = "Verificar_2";
                    comando.Connection = g.Conectar();

                    parametro = new SqlParameter("@Codigo", SqlDbType.Char);
                    parametro.Value = codigo;
                    comando.Parameters.Add(parametro);
                    comando.Parameters.Add("@requisitos",SqlDbType.Char);
                    comando.Parameters["@requisitos"].Value = c[0].ToString();

                    usuario = new SqlDataAdapter(comando);
                    usuario.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                    usuario.Fill(d);

                    foreach (DataRow p in d.Tables[0].Rows)
                    {
                        Verifica v = new Verifica();
                        Verificar.Add(v);
                        if (p[0].ToString().Equals("False"))
                        {
                            Verificar[i].Estado = 0;
                        }
                        else
                        {
                            Verificar[i].Estado = 1;
                        }
                        i++;
                    }
                    g.Desconectar();
                }
            }
            catch (SqlException e)
            {
            }
        }

    }
}