﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrabajoAnalisis_v0._1.Models
{
    public class Verifica
    {
        private int estado;
        public string Aprobado = "Aprobado";
        public string Desaprobado = "Falta o desaprobado";
        public int Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        private string descripcion;

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

    }
}