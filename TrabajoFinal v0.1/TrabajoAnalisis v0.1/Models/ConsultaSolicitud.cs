﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TrabajoAnalisis_v0._1.Generico;

namespace WebApplication3.Models
{
    public class ConsultaSolicitud
    {
        List<DetalleTramite> listaDetalle;
        Solicitud solicitud;
        DataSet dataSet;
        string error;

        public List<DetalleTramite> ListaDetalle { get => listaDetalle; set => listaDetalle = value; }
        public Solicitud Solicitud { get => solicitud; set => solicitud = value; }
        public DataSet DataSet { get => dataSet; set => dataSet = value; }
        public string Error { get => error; set => error = value; }

        public ConsultaSolicitud(string codigo)
        {
            DataSet = obtenerDataSet(codigo);
            verificarConsulta();
            llenarSolicitud();
            llenarDetalleSolicitud();
        }

        public ConsultaSolicitud()
        {
            solicitud = new Solicitud();
            listaDetalle = new List<DetalleTramite>();
        }

        public void llenarSolicitud()
        {
            try
            {
                Solicitud = new Solicitud();

                if (dataSet.Tables.Count > 0)
                {
                    Solicitud.Nombre_solicitante = DataSet.Tables[0].Rows[0]["nombre_persona"].ToString();
                    Solicitud.Asunto = DataSet.Tables[0].Rows[0]["asunto_solicitud"].ToString();
                    Solicitud.Fecha_inicio = DateTime.Parse(DataSet.Tables[0].Rows[0]["fecha_solicitud"].ToString());
                }
            }
            catch (Exception ex)
            {
            }

        }

        public void llenarDetalleSolicitud()
        {
            try
            {
                ListaDetalle = new List<DetalleTramite>();

                DetalleTramite detalleTramite;

                if (dataSet.Tables.Count != 0)
                {
                    foreach (DataRow fila in DataSet.Tables[1].Rows)
                    {
                        detalleTramite = new DetalleTramite()
                        {
                            Nombre_dependencia = fila["nombre_dependencia"].ToString(),
                            Nombre_cargo = fila["nombre_cargo"].ToString(),
                            Nombre_autoridad = fila["nombre_persona"].ToString(),
                            Fecha = DateTime.Parse(fila["fecha"].ToString()),
                            Estado = fila["estado"].ToString(),
                        };

                        ListaDetalle.Add(detalleTramite);
                    }
                }
            }
            catch (Exception ex)
            {
            }

        }

        public DataSet obtenerDataSet(string codigo)
        {
            
            DataSet dataSet = new DataSet();
            GestionarBasedeDatos conexion = new GestionarBasedeDatos();
            SqlDataAdapter SDA;
            SqlCommand comando;
            SqlParameter parametro;

            try
            {
                comando = new SqlCommand();
                comando.CommandType = CommandType.StoredProcedure;
                comando.CommandText = "consultar_datos_solicitud";
                comando.Connection = conexion.Conectar();

                parametro = new SqlParameter("@codigo", SqlDbType.Char);
                parametro.Value = codigo;

                comando.Parameters.Add(parametro);

                SDA = new SqlDataAdapter(comando);
                SDA.Fill(dataSet);
                conexion.Desconectar();
            }
            catch (SqlException e)
            {

            }
            return dataSet;
        }

        public void verificarConsulta()
        {
            if(DataSet.Tables.Count == 0)
            {
                Error = "No se hallo el expediente";
            }
        }
    }
}