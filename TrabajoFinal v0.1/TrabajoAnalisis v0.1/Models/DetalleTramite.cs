﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplication3.Models
{
    public class DetalleTramite
    {
        string nombre_dependencia, nombre_cargo, nombre_autoridad, estado;
        DateTime fecha;

        public string Nombre_dependencia { get => nombre_dependencia; set => nombre_dependencia = value; }
        public string Nombre_cargo { get => nombre_cargo; set => nombre_cargo = value; }
        public string Nombre_autoridad { get => nombre_autoridad; set => nombre_autoridad = value; }
        public string Estado { get => estado; set => estado = value; }
        public DateTime Fecha { get => fecha; set => fecha = value; }
    }
}