﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TrabajoAnalisis_v0._1.Generico;

namespace WebApplication3.Models
{
    public class AceptarSolicitud
    {
        DataSet dataSet;
        Solicitud solicitud;
        string error;
        GestionarBasedeDatos conexion;

        public DataSet DataSet { get => dataSet; set => dataSet = value; }
        public string Error { get => error; set => error = value; }
        public Solicitud Solicitud { get => solicitud; set => solicitud = value; }

        public AceptarSolicitud(string codigo)
        {
            conexion = new GestionarBasedeDatos();
            DataSet = obtenerDataSet(codigo);
            verificarConsulta();
            
            //llenarSolicitud();
            //llenarDetalleSolicitud();
        }

        public AceptarSolicitud()
        {
            conexion = new GestionarBasedeDatos();
            solicitud = new Solicitud();
            //listaDetalle = new List<DetalleTramite>();
        }

        public DataSet obtenerDataSet(string codigo)
        {
            DataSet dataSet = new DataSet();
            SqlDataAdapter SDA;
            SqlCommand comando;
            SqlParameter parametro;

            try
            {

                comando = new SqlCommand();
                comando.CommandType = CommandType.StoredProcedure;
                comando.CommandText = "Consultar_Solicitudes_Pendientes";
                comando.Connection = conexion.Conectar();

                parametro = new SqlParameter("@Id_facultad", SqlDbType.Char);
                parametro.Value = codigo;

                comando.Parameters.Add(parametro);

                SDA = new SqlDataAdapter(comando);
                SDA.Fill(dataSet);
                conexion.Desconectar();
            }
            catch (SqlException e)
            {

            }
            return dataSet;
        }

        public void verificarConsulta()
        {
            if (DataSet.Tables.Count == 0)
            {
                Error = "No se hallo el expediente";
            }
        }


    }
}