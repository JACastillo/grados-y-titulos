﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TrabajoAnalisis_v0._1.Generico;

namespace TrabajoAnalisis_v0._1.Models
{
    public class Usuario
    {
        public string Contraseña,Nombre,Nombre2,nivel,Grupo;
        GestionarBasedeDatos g;

        public string Nivel { get => nivel; set => nivel = value; }

        public Usuario(string Nombre, string Contraseña)
        {
            g = new GestionarBasedeDatos();
            this.Nombre = Nombre;
            this.Contraseña = Contraseña;
        }
        public DataSet RellenarData()
        {
            DataSet d = new DataSet();
            SqlDataAdapter usuario;
            SqlCommand comando;
            SqlParameter parametro;
            try
            {
                comando = new SqlCommand();
                comando.CommandType = CommandType.StoredProcedure;
                comando.CommandText = "consultar_usuario";
                comando.Connection = g.Conectar();

                parametro = new SqlParameter("@Id_usuario", SqlDbType.Char);
                parametro.Value = Nombre;

                comando.Parameters.Add(parametro);

                usuario = new SqlDataAdapter(comando);
                usuario.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                usuario.Fill(d);
                g.Desconectar();
            }
            catch (SqlException e)
            {

            }
            return d;
        }

        public bool Ingresarusuario()
        {
            string  pa=null, nivel;
            bool act = false;
            DataSet t;
            SqlConnection s;
            SqlCommand cmd = new SqlCommand();
            s = g.Conectar();
            t = RellenarData();
            foreach (DataRow c in t.Tables[0].Rows)
            {
                pa = c[0].ToString();
                if (pa!=null)
                {
                    Nombre2 = c[1].ToString();
                    Nivel = c[2].ToString();
                    Grupo = c[3].ToString();
                    act = true;
                }
            }
            g.Desconectar();
            return act;
        }
    }
}