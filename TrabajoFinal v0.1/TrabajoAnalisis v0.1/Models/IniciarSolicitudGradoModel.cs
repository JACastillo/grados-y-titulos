﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TrabajoAnalisis_v0._1.Generico;

namespace SIGRAT.Models
{
    public class IniciarSolicitudGradoModel
    {
        public string nroExpediente;
        public string mensaje;

        public IniciarSolicitudGradoModel(string usernName)
        {
            try
            {
                string codigoAlumno = ObtenerCodigoEstudiante(usernName);
                DataSet nroExpDataSet = ConsultarNroExpediente(codigoAlumno);
                Console.WriteLine(nroExpDataSet.Tables[0].Rows[0]["Numero_de_expediente"].ToString());
                if (nroExpDataSet.Tables.Count == 0)
                {
                    System.Diagnostics.Debug.WriteLine("El alumno no tiene solicitud existente, se procederá a crear una");
                    nroExpDataSet = RegistrarNuevaSolicitudGrado(codigoAlumno);
                    mensaje = "Solicitud registrada con éxito";
                    nroExpediente = nroExpDataSet.Tables[0].Rows[0]["Numero_de_expediente"].ToString();
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Solicitud existente encontrada");
                    nroExpediente= nroExpDataSet.Tables[0].Rows[0]["Numero_de_expediente"].ToString();
                    mensaje = "Ya existe una solicitud en curso";
                }
            }
            catch (Exception e) { }

            
        }

        private string ObtenerCodigoEstudiante(string usernName)
        {
            DataSet dataSet = new DataSet();
            GestionarBasedeDatos conexion = new GestionarBasedeDatos();
            SqlDataAdapter SDA;
            SqlCommand comando;
            SqlParameter parametro;
            string h="";

            try
            {

                comando = new SqlCommand();
                comando.CommandType = CommandType.StoredProcedure;
                comando.CommandText = "obtener_codigo_estudiante";
                comando.Connection = conexion.Conectar();

                parametro = new SqlParameter("@username", SqlDbType.VarChar);
                parametro.Value = usernName;

                comando.Parameters.Add(parametro);

                SDA = new SqlDataAdapter(comando);
                SDA.Fill(dataSet);
                conexion.Desconectar();
                h= dataSet.Tables[0].Rows[0]["Codigo_estudiante"].ToString();

            }
            catch (SqlException e)
            {
                System.Diagnostics.Debug.WriteLine($"ConsultarNroExpediente dice: {e.Message}");
            }
            return h;
        }

        public DataSet ConsultarNroExpediente(string codigoAlumno)
        {
            DataSet dataSet = new DataSet();
            GestionarBasedeDatos conexion = new GestionarBasedeDatos();
            SqlDataAdapter SDA;
            SqlCommand comando;
            SqlParameter parametro;

            try
            {

                comando = new SqlCommand();
                comando.CommandType = CommandType.StoredProcedure;
                comando.CommandText = "consultar_nro_expediente";
                comando.Connection = conexion.Conectar();

                parametro = new SqlParameter("@codAlumno", SqlDbType.Char);
                parametro.Value = codigoAlumno;

                comando.Parameters.Add(parametro);

                SDA = new SqlDataAdapter(comando);
                SDA.Fill(dataSet);
                conexion.Desconectar();
            }
            catch (SqlException e)
            {
                System.Diagnostics.Debug.WriteLine($"ConsultarNroExpediente dice: {e.Message}");
            }

            return dataSet;
        }

        private DataSet RegistrarNuevaSolicitudGrado(string codigoAlumno)
        {
            DataSet dataSet = new DataSet();
            GestionarBasedeDatos conexion = new GestionarBasedeDatos();
            SqlDataAdapter SDA;
            SqlCommand comando;
            SqlParameter parametro;

            try
            {
                comando = new SqlCommand();
                comando.CommandType = CommandType.StoredProcedure;
                comando.CommandText = "registrar_nueva_solicitud_grado";
                comando.Connection = conexion.Conectar();

                parametro = new SqlParameter("@codAlumno", SqlDbType.Char);
                parametro.Value = codigoAlumno;

                comando.Parameters.Add(parametro);

                SDA = new SqlDataAdapter(comando);
                SDA.Fill(dataSet);
                conexion.Desconectar();
            }
            catch (SqlException e)
            {
                System.Diagnostics.Debug.WriteLine($"RegistrarNuevaSolicitudGrado dice: {e.Message}");
            }

            return dataSet;
        }
    }
}