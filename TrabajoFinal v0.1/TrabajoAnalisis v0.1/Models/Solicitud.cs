﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication3.Models
{
    public class Solicitud
    {
        string nombre_solicitante, asunto;
        DateTime fecha_inicio;

        public Solicitud()
        {
            fecha_inicio = new DateTime(1900, 1, 1);
        }

        public string Nombre_solicitante { get => nombre_solicitante; set => nombre_solicitante = value; }
        public string Asunto { get => asunto; set => asunto = value; }
        public DateTime Fecha_inicio { get => fecha_inicio; set => fecha_inicio = value; }
    }
}